from django import forms
from django.forms import ModelForm

# from markdownx.fields import MarkdownxFormField

from .models import Question, Answer, Comments
from crispy_forms.helper import FormHelper


class QuestionForm(ModelForm):

    class Meta:
        model = Question
        fields = ('title',)


class AnswerForm(ModelForm):

    answer_content = forms.CharField(max_length=200, widget=forms.Textarea,
                                     label='')
    question_id = forms.CharField(max_length=200, widget=forms.HiddenInput())

    class Meta:
        model = Answer
        fields = ('answer_content', 'question_id')


class CommentForm(ModelForm):
    comment_content = forms.CharField(max_length=200,
                                      label='', widget=forms.TextInput(
                                        attrs={
                                            'placeholder': 'comment here...'}))

    class Meta:
        model = Comments
        fields = ('comment_content',)
