from django.shortcuts import render
from .models import Question, Answer
from .forms import QuestionForm, AnswerForm, CommentForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages

# from django.http import HttpRequest

# Create your views here.


def question_list(request):
    template_name = 'qa/home.html'
    if request.method == 'POST':
        question_form = QuestionForm(request.POST)
        if question_form.is_valid():
            question = question_form.save(commit=False)
            question.user = request.user
            question.save()
            return redirect("qa:home")
        else:
            messages.error(request, 'Question already exists or invalid input')
            return redirect('qa:home')
    else:
        question_form = QuestionForm(request.POST)
        questions = Question.objects.prefetch_related('answer')
        return render(
            request, template_name, {'questions': questions,
                                     'form': question_form,
                                     })


@login_required
def add_answer(request):
    template_name = 'qa/answers.html'
    if request.method == 'POST':
        question_id = request.POST.get('question_id')
        answer_form = AnswerForm(request.POST)
        if answer_form.is_valid():
            answer = answer_form.save(commit=False)
            answer.user = request.user
            answer.question = Question.objects.get(id=question_id)
            answer.save()
        return redirect("qa:add_answer")
    else:
        question_form = QuestionForm()
        answer_form = AnswerForm()
        questions = Question.objects.all()
        for question in questions:
            question.first_answer = question.answer.first()
        return render(
            request, template_name, {'questions': questions,
                                     'form': question_form,
                                     'answer': answer_form
                                     })


@login_required
def question_detail(request, pk):
    template_name = 'qa/question_detail.html'
    if request.method == 'POST':
        answer_form = AnswerForm(request.POST)
        if answer_form.is_valid():
            answer = answer_form.save(commit=False)
            answer.user = request.user
            answer.question = Question.objects.get(id=pk)
            answer.save()
            return redirect('qa:question_detail', pk=pk)
        else:
            messages.error(request, 'Invalid input')
            return redirect('qa:question_detail', pk=pk)
    else:
        answer_form = AnswerForm()
        question = Question.objects.get(id=pk)
        answers = Answer.objects.filter(question=question)
        question.no_of_answers = Answer.objects.filter(
            question=question).count()

        for answer in answers:
            answer.list_of_comments = answer.comments.all()
        comment_form = CommentForm()
        return render(request, template_name, {'selected_question': question,
                                               'answer_form': answer_form,
                                               'answers': answers,
                                               'comment_form': comment_form,
                                               })


@login_required
def handle_comments(request, pk):
    if request.method == 'POST':
        # id = request.POST.get('answer_id')
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.user = request.user
            comment.answer = Answer.objects.get(id=pk)
            comment.save()
            qid = comment.answer.question.id
            return redirect("qa:question_detail", pk=qid)


@login_required
def search(request):
    status = {}
    if request.method == 'POST':
        search_data = request.POST.get('search')
        status = Question.objects.filter(title__icontains=search_data)

        for question in status:
            question.no_of_answers = len(question.answer.all())
            question.first_answer = question.answer.first()
    return render(request, "qa/search.html", {"status": status})
