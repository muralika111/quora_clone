from django.conf.urls import url
from . import views

app_name = 'qa'

urlpatterns = [
    url(r'^$', views.question_list, name='home'),
    url(r'^answers/$', views.add_answer, name='add_answer'),
    url(r'^question/(?P<pk>\d+)/$', views.question_detail,
        name='question_detail'),
    url(r'^search/$', views.search,
        name='search'),
    url(r'^comment/(?P<pk>\d+)/$', views.handle_comments,
        name='comment')
]
